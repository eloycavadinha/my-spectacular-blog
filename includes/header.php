<!DOCTYPE html> 
<html>
<head>
<meta charset="utf-8">
    <title>Albelli - My Spectacular Blog</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/script.js"></script>
</head>
<body>
    <main class="main-frame">
        <div class="headline">
            <h1>My spectacular blog</h1>
            <p>A totally false statement</p>
        </div>

        <div class="container-new-posts">
            <div class="new-post-header">New blog post</div>
            <div class="form">
                <div class="fields">
                    <input id="title" type="text" name="title" placeholder="My post title" onblur="validateEmptyField('title')">
                    <textarea id="content" name="content" placeholder="Here all my important post text!" onblur="validateEmptyField('content')"></textarea>
                    <input id="email" type="email" name="email" placeholder="Email Address" value="escneto@gmail.com" onblur="validateEmail()">
                    <input id="file" name="file" type="file" value="Upload image" accept=".png, .jpg" style="border:none;"/>
                    <span class="clear-image" onclick="cleanFileUploader()">(x remove image)</span>
                </div>
                <div class="save-button">
                    <button id="submit-post">Save post</button>
                </div>
            </div>
        </div>