<?php
/**
 * Created by PhpStorm.
 * User: Eloy Cavadinha
 * Date: 03/04/2018
 * Time: 18:09
 */

function debug_print($var) {
    print_r("<pre>");
    print_r($var);
    print_r("<pre>");
    exit;
}

require_once "engine/ListArticles.php";
$dirReader = new ListArticles();

$articles_list = $dirReader->readArticleDir();
$most_used_words = $dirReader->getMostUsedWords();

include "includes/header.php";
?>
    <div class="container-articles">
        <div class="most-used-words">
            <p>Most used words here</p>
            <ul id="most-used-words">
                <?php
                if(empty($most_used_words))
                {
                    echo "No posts were found";
                }
                else
                {
                    foreach ($most_used_words as $word)
                    {
                        echo "<li>$word</li>";
                    }
                }
                ?>
            </ul>
        </div>
        <div class="articles">
            <?php
            //showtime!

            if(empty($articles_list))
            {
                echo "No posts were found";
            }
            else
            {
                foreach ($articles_list as $articles)
                {
                    echo "<article>";
                    echo "<h2>" . $articles['title'] . "<span>" . $articles['date'] . "</span></h2>";
                    echo $articles['content'] . "<hr><div>By: <em>". $articles['author'] . "</em></div>";
                    echo "</article>";
                }
            }
            ?>
        </div>
    </div>

<?php include("includes/footer.php"); ?>