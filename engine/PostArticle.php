<?php
/**
 * Created by PhpStorm.
 * User: Eloy Cavadinha
 * Date: 03/04/2018
 * Time: 17:43
 */

class NotAuthorizedException extends Exception {}


/**
 * Class PostArticle
 *
 * Saves an article as .txt in the 'articles' folder
 * Only words with more than 4 characteres will be saved in the content
 * Images will be stored in 'articles/images/'
 *
 * Will return data of article when it is saved.
 */
class PostArticle
{
    const ARTICLES_DIRECTORY = '../articles/'; //@TODO create a config file
    const OWNER_EMAIL = 'escneto@gmail.com'; //@CODE_SMELL - create a config file

    /**
     * @var string static separator of the meta data and post content
     */
    private static $articleSeparator = '_#_post!content_#_';
    /**
     * @var title of the article
     */
    private $title;
    /**
     * @var content of the article
     */
    private $content;
    /**
     * @var email of the blog owner
     */
    private $email;
    /**
     * @var author of the article post (not present in the front-end)
     */
    private $author;
    /**
     * @var DateTime object
     */
    private $objDate;
    /**
     * @var Image of the post
     */
    private $featured_image;

    /**
     * PostArticle constructor.
     * @param $title
     * @param $content
     * @param $email
     * @param string $featured_image
     * @param string $author
     */
    public function __construct($title, $content, $email, $featured_image = '', $author = 'Eloy Cavadinha')
    {
        $objDate = new DateTime('now');

        $this->setTitle($title);
        $this->setContent($content);
        $this->setEmail($email);
        $this->setAuthor($author);
        $this->setDate($objDate);
        $this->setFeaturedImage($featured_image);
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }


    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->objDate;
    }

    /**
     * @param mixed $objDate
     */
    public function setDate($objDate)
    {
        $this->objDate = $objDate;
    }

    /**
     * @return mixed
     */
    public function getFeaturedImage()
    {
        return $this->featured_image;
    }

    /**
     * @param mixed $featured_image
     */
    public function setFeaturedImage($featured_image)
    {
        $this->featured_image = $featured_image;
    }

    /**
     * Saves the article in the file system as txt
     *
     * @return array containing the article data
     * @throws NotAuthorizedException
     */
    function doPost()
    {
        if ($this->getEmail() === self::OWNER_EMAIL)
        {
            if(!is_dir(self::ARTICLES_DIRECTORY))
                mkdir(self::ARTICLES_DIRECTORY);

            $article_content =  $this->cleanPostContent($this->getContent());
            $file_path = self::ARTICLES_DIRECTORY  . $this->objDate->format('YmdHis') . '-' . $this->getTitle() . '.txt';

            $fileHandler = fopen($file_path, 'w');
            if (!$fileHandler)
            {
                echo "Can`t write files in folder. Check folder permissions.";
                exit;
            }
            else
            {
                fwrite($fileHandler, $this->createJsonMetaData());
                fwrite($fileHandler, self::$articleSeparator);
                fwrite($fileHandler, $article_content);
                fclose($fileHandler);

                return array
                (
                    'title' => $this->getTitle(),
                    'author' => $this->getAuthor(),
                    'email' => $this->getEmail(),
                    'image_path' => $this->getFeaturedImage(),
                    'date' => $this->objDate->format('Y-m-d'),
                    'content' => $article_content
                );
            }
        }
        else
        {
            $error = 'Wrong e-mail address';
            throw new NotAuthorizedException($error);
        }
    }

    /**
     * Creates a json meta data for the article
     *
     * @return string
     */
    private function createJsonMetaData()
    {
        $json_meta_data = array
        (
            'title' => $this->getTitle(),
            'author' => $this->getAuthor(),
            'email' => $this->getEmail(),
            'image_path' => $this->getFeaturedImage(),
            'date' => $this->objDate->format('Y-m-d')
        );

        return json_encode($json_meta_data);
    }

    /**
     * Clean the post content.
     * All the words that are longer than 4 characters are saved
     *
     * @param $contentString
     * @param int $wordLength
     * @return mixed
     */
    private function cleanPostContent($contentString, $wordLength = 4)
    {
        /** foreach solution */
//        $all_words = explode(" ", $contentString);
//        foreach($all_words as $index => $word) {
//            if(mb_strlen($word) <= $wordLength) unset($all_words[$index]);
//        }
//        $cleanString = implode(" ", $all_words);
//        return $cleanString;

        /** regex solution */
        $cleanStringNoSpecialChar = preg_replace('/[^A-Za-z0-9\- ]/', '', $contentString);
        $cleanString = preg_replace('~\b[a-z]{1,'.$wordLength.'}\b\s*~', '', $cleanStringNoSpecialChar);
        if (empty($cleanString))
        {
            echo "There are not enought big words in the post content";
            exit;
        }
        return preg_replace('~\b[a-z]{1,4}\b\s*~', '', $contentString);
    }

}

//The post was submitted by an authorized source, i.e. the blog frontend
if ((isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) &&
    strtolower(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST)) === strtolower($_SERVER['HTTP_HOST'])
){
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST))
    {
        require_once "ListArticles.php";
        $objPostArticle = new PostArticle($_POST['title'], $_POST['content'], $_POST['email']);
        $objListArticles = new ListArticles();
        try
        {
            $brandNewPost = $objPostArticle->doPost();
            $objListArticles->readArticleDir($objPostArticle::ARTICLES_DIRECTORY);
            $mostUsedWords = $objListArticles->getMostUsedWords();
            echo json_encode(array('newPost' => $brandNewPost, 'mostUsedWords' => $mostUsedWords));
        }
        catch (NotAuthorizedException $e)
        {
            echo $e;
        }
        exit;
    }
}
else
{
    die("Could not verify the referer");
}