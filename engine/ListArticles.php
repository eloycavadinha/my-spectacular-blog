<?php
/**
 * Created by PhpStorm.
 * User: Eloy Cavadinha
 * Date: 03/04/2018
 * Time: 18:19
 */

/**
 * Class ListArticles
 *
 * Read the directory searching for articles.
 * Can return the list of articles and the most used words.
 */
class ListArticles
{
    const ARTICLES_DIRECTORY = 'articles/'; //@TODO create a config file
    /**
     * @var array
     */
    private $most_used_word = array();

    /**
     * ListArticles constructor.
     */
    public function __construct()
    {
        if(!is_dir(self::ARTICLES_DIRECTORY))
        {
            mkdir(self::ARTICLES_DIRECTORY, 0755, true);
        }
    }

    /**
     * Return the most used words from most used to less used (default: 5 words)
     *
     * @param int $amountWords - How many words we want.
     * @return array
     */
    public function getMostUsedWords($amountWords = 5)
    {
        $all_words = $this->most_used_word;
        arsort($all_words);


        $limit = 0;
        $chosen_words = array();
        foreach($all_words as $word => $count)
        {
            if ($limit === $amountWords)
                break;

            $chosen_words[] = $word;
            $limit++;
        }

        return $chosen_words;
    }

    /**
     * Scan the directory to check for articles
     *
     * @param string $directory - Directory that will be scanned
     * @return array
     */
    public function readArticleDir($directory = '')
    {
        //all articles will be listed here
        $articles_list = array();
        $baseDir = empty($directory) ? self::ARTICLES_DIRECTORY : $directory;

        $directory_files = scandir($baseDir);
        arsort($directory_files);
        foreach ($directory_files as $filename)
        {
            if (!in_array($filename, array('.','..')))
            {
                //@CODE_SMELL use splinfo to get file type
                $extension = strtolower(substr($filename, -4));
                if($extension === ".txt") //@TODO MD parser
                {
                    $opened_file = fopen($baseDir . $filename, 'r');

                    //explode the file contents to split the meta json and the article content
                    $file_content = explode("_#_post!content_#_", stream_get_contents($opened_file));

                    $json_meta_data = json_decode(array_shift($file_content), true);
                    $article_content = array_shift($file_content);

                    $articles_list[$filename] = array
                    (
                        'title' => $json_meta_data['title'],
                        'author' => $json_meta_data['author'],
                        'date' => $json_meta_data['date'],
                        'featured_image' => isset($json_meta_data['image']) ? isset($json_meta_data['image']) : '',
                        'content' => $article_content
                    );

                    //register all words for later
                    $this->registerWords($article_content);
                }
            }
        }

        return $articles_list;
    }

    /**
     * Register all words in the class variable.
     * @param $postContent - String containing the post content
     *
     * @code_smell - I could use session for that or save it in another .txt file
     */
    private function registerWords($postContent)
    {
        $clean_content_words = explode(" ", strip_tags($postContent));
        foreach($clean_content_words as $word)
        {
            $clean_word = ucfirst(preg_replace('/[^A-Za-z0-9\-]/', '', $word));
            if (isset($this->most_used_word[$clean_word]))
            {
                $this->most_used_word[$clean_word]++;
            }
            else if (strlen($clean_word) > 3)
            {
                $this->most_used_word[$clean_word] = 1;
            }
        }
    }
}
?>
