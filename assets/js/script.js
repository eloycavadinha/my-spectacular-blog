/**
 * Created by Eloy Cavadinha on 03/04/2018.
 */
function validate()
{
    if (!validateEmptyField('title')) return false;
    if (!validateEmptyField('content')) return false;
    if (!validateEmail()) return false;

    submitPost();
}

function submitPost()
{
    $.post("engine/PostArticle.php",
        {
            title: $('#title').val(),
            content: $('textarea#content').val(),
            email: $('#email').val()
        }
    ).done(function( data ) {
            if(data) {
                try {
                    var response = JSON.parse(data);
                    console.log('response', response);
                    createArticleHtml(response.newPost);
                    updateMostUsedWords(response.mostUsedWords);
                } catch(e) {
                    alert(data);
                }
            }
        }
    );
}

function createArticleHtml(brandNewPost)
{
    var html = "<article>";
    html += "<h2>" + brandNewPost.title + "<span>" + brandNewPost.date + "</span></h2>";
    html += brandNewPost.content + "<hr><div>By: <em>"+ brandNewPost.author + "</em></div>";
    html += "</article>";

    $('.articles').prepend(html);
}

function updateMostUsedWords(mostUsedWords)
{
    console.log('mostUsedWord',mostUsedWords);
    var li_html = "";
    for (var i = 0; i < mostUsedWords.length; i++)
    {
        li_html += "<li>"+mostUsedWords[i]+"</li>";
    }

    $('#most-used-words').html(li_html);
}

function cleanFileUploader()
{
    $('#image-file').val('');
}

function isEmpty(value)
{
    return ( ( value.trim=="") || (value==0) ) ? true : false;
}

function validateEmptyField(id)
{
    if ((isEmpty($('#'+id).val()))) {
        $('#'+id).css('border-color','red');
        return false;
    } else {
        $('#'+id).css('border-color','');
        return true;
    }
}

function validateEmail()
{
    email = $('#email').val();
    if (email != 'escneto@gmail.com') {
        $('#email').css('border-color','red');
        return false;
    } else {
        $('#email').css('border-color','');
        return true;
    }
}

$( document ).ready(function() {
    $( "#submit-post" ).click(function() {
        validate();
    });
});